/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bankersalgorithm;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author naina
 */
public class BankersAlgorithm {
    
    static final int N = 5;
    static final int[] res_exist = new int[]{6,3,2,7,1};
    static final int[] res_avail = new int[]{6,3,2,7,1};
    static int[] res_req = new int[N];
    static int[] res_curr = new int[N];
    static ArrayList<int[]> req_list = new ArrayList<>();
    static ArrayList<int[]> curr_list = new ArrayList<>();
   
    public static void main(String[] args) {
        
        boolean flag = true;
        
        Scanner c = new Scanner(System.in);
        
        //Existing resorces
        System.out.println("The number of available resources are as follows");
        System.out.println("Resource 0: "+res_exist[0]);
        System.out.println("Resource 1: "+res_exist[1]);
        System.out.println("Resource 2: "+res_exist[2]);
        System.out.println("Resource 3: "+res_exist[3]);
        System.out.println("Resource 4: "+res_exist[4]);
        
        while(flag)
        {
            //Enter the Current resource for each client
            System.out.println("Enter current resource allocation");
            String data = c.nextLine();
            String output[] = data.split(" ");
            res_curr[0] = Integer.parseInt(output[0]);
            res_curr[1] = Integer.parseInt(output[1]);
            res_curr[2] = Integer.parseInt(output[2]);
            res_curr[3] = Integer.parseInt(output[3]);
            res_curr[4] = Integer.parseInt(output[4]);
            
            //check if the entered current requirement is valid
            int result = validate(res_curr);
            if(result == -1)
            {
                flag = true;
                continue;
            }
            
            //update the available resources
            for(int i = 0; i< res_avail.length; i++)
            {
                res_avail[i] = res_avail[i] - res_curr[i];
            }
            
            //add the current resource requirenment to the current list
            curr_list.add(res_curr);
            
            //Enter the maximum resource requirenment
            System.out.println("Enter required resources");
            String data_max = c.nextLine();
            String output_max[] = data_max.split(" ");
            res_req[0] = Integer.parseInt(output_max[0]);
            res_req[1] = Integer.parseInt(output_max[1]);
            res_req[2] = Integer.parseInt(output_max[2]);
            res_req[3] = Integer.parseInt(output_max[3]);
            res_req[4] = Integer.parseInt(output_max[4]);
            
            //add the required resources to the requirement list
            req_list.add(res_req);
            
            System.out.println("Do you wish to enter more resource requirements for next client? (Enter YES or NO)");  
            if(c.nextLine().equals("NO"))
            {
                flag = false;
            }
        }
        
        //Compute the resource allocation for each client 
        compute(curr_list,req_list);
    }

    private static void compute(ArrayList<int[]> curr_list,ArrayList<int[]> req_list) {
        
        int count = 0;
        
        //check for each process in the current list
        for(int p = 0; p < curr_list.size(); p++)
        {
        
            for (int i = 0; i < curr_list.size(); i++) {
                
                int[] req_data = req_list.get(i);
                
                //check with the available resources 
                for (int j = 0; j < res_avail.length; j++) {
                    
                   if(req_data[j] != -1)
                   {
                        //Compare the resource requirenment with the available resources
                        if(req_data[j] <= res_avail[j])
                        {
                            //if requirement is less than available resource increment count
                            count++;
                        }
                    }
                }
            
                //if all the resource requirenment are valid and can be satisfied
                if(count == 5)
                {
                    //request granted
                    System.out.println("Request granted and process completed for client "+i);
                
                    //add the resources held by client to the available resources after completion
                    for(int k = 0; k < res_avail.length; k++)
                    {
                        int[] curr_data = curr_list.get(i);
                        res_avail[k] = res_avail[k] + curr_data[k];  
                        res_req[k] = -1;
                    }   
                    req_list.set(i, res_req);
                }
            }
            count = 0;
        }
        
        for(int i = 0; i < req_list.size(); i++)
        {
            int[] curr_data = req_list.get(i);
            if(curr_data[i] != -1)
            {
                System.out.println("Resource requirenment for client" +i+ "is not satisfied");
                System.out.println("Hence the system is deadlocked");
            }
        }
   
    }
    
    //validate if the entered current resources is less than available resources
    private static int validate(int[] res_curr) {
        
        int error = 0;
        for(int i = 0; i < res_curr.length; i++)
        {
            if(res_curr[i] > res_avail[i])
            {
                System.out.println("The current resource requirement for resoure "+i+ "is greater than available resource");
                error = 1;
            }
        }
        if(error == 1)
        {
            return -1;
        }
        else
        {            
            return 0;
        }
    }
    
}
